# Detective Mystery - C# Pattern Matching Activity

## Overview

In this Detective Mystery activity, you will dive into C# pattern matching and enhance your skills by solving a fictional detective case. Follow the steps below to unravel the mystery while practicing pattern matching.

## Instructions

1. **Create Clue and Suspect Classes**

   - Implement two classes, `Clue` and `Suspect`.
   - The `Clue` class should have a property `Information` of type string.
   - The `Suspect` class should have properties: `Name` (string), `Age` (int?), and `Alibi` (string).

2. **Pattern Matching Function**

   - Write a method called `IsClueOrSuspect` that takes an `object` as a parameter.
   - Use pattern matching to determine if the object is an instance of `Clue` or `Suspect`.
   - Return a string indicating the type ("Clue" or "Suspect") or "Unknown" for other objects.
   - Test this method with instances of `Clue`, `Suspect`, and an unrelated object.

3. **Clue Analysis**

   - Create an expression-bodied method called `AnalyzeClue` that takes a `Clue` as a parameter.
   - Inside this method, check if the `Information` property of the clue has more than 10 characters.
   - Return "Relevant Clue" if the condition is met, otherwise return "Irrelevant."

4. **Suspect Categorization**

   - Implement a method named `CategorizeSuspect` using a switch expression.
   - This method should categorize a `Suspect` based on their `Age`.
   - Classify the suspect as "Young" if their age is less than 30, "Middle-aged" if between 30 and 50, and "Senior" if above 50.
   - Test this method with various `Suspect` instances.

## Objective

By completing this Detective Mystery activity, you will gain a solid understanding of C# pattern matching and how to differentiate between different types of objects. You'll also practice working with classes and methods in C#.

## Solving the Mystery

Follow the instructions step by step to solve the mystery while applying pattern matching concepts. Enjoy the detective-themed coding adventure!

## Note

This activity is designed to provide a practical and engaging way to learn C# pattern matching. It offers a unique and fun approach to mastering this essential programming skill.
