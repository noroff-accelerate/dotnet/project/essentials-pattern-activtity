﻿namespace Noroff.Samples.EssentialsPatternMatchingActivity
{
    using System;

    class Clue
    {
        public string Information { get; set; }
    }

    class Suspect
    {
        public string Name { get; set; }
        public int? Age { get; set; }
        public string Alibi { get; set; }
    }

    class Program
    {
        static string IsClueOrSuspect(object obj)
        {
            switch (obj)
            {
                case Clue _:
                    return "Clue";
                case Suspect _:
                    return "Suspect";
                default:
                    return "Unknown";
            }
        }

        static string AnalyzeClue(Clue clue)
        {
            return clue.Information.Length > 10 ? "Relevant Clue" : "Irrelevant";
        }

        static string CategorizeSuspect(Suspect suspect)
        {
            return suspect.Age switch
            {
                < 30 => "Young",
                >= 30 and <= 50 => "Middle-aged",
                > 50 => "Senior",
                _ => "Unknown"
            };
        }

        static void Main(string[] args)
        {
            Clue relevantClue = new Clue { Information = "This is an important clue." };
            Clue irrelevantClue = new Clue { Information = "Small clue" };
            Suspect youngSuspect = new Suspect { Name = "Alice", Age = 25 };
            Suspect middleAgedSuspect = new Suspect { Name = "Bob", Age = 40 };
            Suspect seniorSuspect = new Suspect { Name = "Charlie", Age = 60 };
            string unrelatedString = "Not a clue or suspect";

            Console.WriteLine(IsClueOrSuspect(relevantClue)); // Output: Clue
            Console.WriteLine(IsClueOrSuspect(irrelevantClue)); // Output: Clue
            Console.WriteLine(IsClueOrSuspect(youngSuspect)); // Output: Suspect
            Console.WriteLine(IsClueOrSuspect(middleAgedSuspect)); // Output: Suspect
            Console.WriteLine(IsClueOrSuspect(seniorSuspect)); // Output: Suspect
            Console.WriteLine(IsClueOrSuspect(unrelatedString)); // Output: Unknown

            Console.WriteLine(AnalyzeClue(relevantClue)); // Output: Relevant Clue
            Console.WriteLine(AnalyzeClue(irrelevantClue)); // Output: Irrelevant

            Console.WriteLine(CategorizeSuspect(youngSuspect)); // Output: Young
            Console.WriteLine(CategorizeSuspect(middleAgedSuspect)); // Output: Middle-aged
            Console.WriteLine(CategorizeSuspect(seniorSuspect)); // Output: Senior
        }
    }

}